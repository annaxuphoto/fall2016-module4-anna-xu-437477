import sys, os, re, operator
 
if len(sys.argv) < 2:
	sys.exit("Usage: %s filename" % sys.argv[0])
 
filename = sys.argv[1]
at_bats = {}
hits = {}
batting_avg = {}
match = ""
 
if not os.path.exists(filename):
	sys.exit("Error: File '%s' not found" % sys.argv[1])
    
f = open(filename)
for line in f:
    fullname_regex = re.compile(r"\b[A-Za-z]{2,20}[ ]{1}[A-Za-z]{2,20}\b")
    match_obj = re.match(fullname_regex, line)
    if match_obj:
        match = match_obj.group()
    number_regex = re.compile(r"\b[0-9]\b")
    nums = number_regex.findall(line)
    if match is not None and len(nums) == 3:
        if match in at_bats and at_bats[match]:
            at_bats[match] += float(nums[0])
            hits[match] += float(nums[1])
        else:
            at_bats[match] = float(nums[0])
            hits[match] = float(nums[1])
    
f.close()

if len(at_bats) == len(hits):
    for name, num in hits.items():
        if at_bats[name] > 0:
            batting_avg[name] = float(num) / float(at_bats[name])

better_batter = sorted(batting_avg.items(), key=operator.itemgetter(1), reverse=True)

for (key, val) in better_batter:
    print key, ": ", "{:.3f}".format(val)